It's a simple React Native project that shows a bug in react-redux 6.0.0.
The core problem is that mapStateToProps doesn't push updates when Store's state
changes. For more information, visit: https://github.com/reduxjs/react-redux/issues/1145
