import React, { Component } from 'react';
import { createStore, combineReducers } from 'redux';
import { Provider, connect } from "react-redux";
import { Text } from 'react-native';

// ACTIONS
const getHomePageTextbooks = () => {
  return {
    type: 'GET_DATA'
  };
};

// REDUCER
const INITIAL_STATE = {
  data: []
};

const HomePageTextbooks = (previousState = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'GET_DATA':
      return { data: [1,2,3,4,5,6,7] };
    default:
      return previousState;
  }
};

// STORE
const store = createStore(HomePageTextbooks);
setTimeout(() => { console.log("LAST STORE'S STATE: ", store.getState())}, 3000);

// REACT NATIVE ELEMENT
class DumbElement extends Component {
  constructor(props) {
    super(props);
    this.props.getHomePageTextbooks();
  }

  // @BUG: SHOULD UPDATE to '7'. IT NEVER HAPPENS
  render() {
    return <Text style={{ marginLeft: 50, marginTop: 100 }}>{this.props.data.length}</Text>
  }
}

const DumbElementRedux =
  connect(({ data }) => ({ data }), { getHomePageTextbooks })(DumbElement);

// APP
export default App = (props) => {
  return (
    <Provider store={store}>
      <DumbElementRedux />
    </Provider>
  )
}
